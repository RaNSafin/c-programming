#include <iostream>

#include "mergeSorting.h"
#include "pow.h"
#include "reverseSequence.h"


long long recursivePow(int digit, int degree)
{
    if(degree == 0)
    {
        return 1;

    }

    if(degree & 1)
    {
        return digit*recursivePow(digit,degree-1);
    } else
    {
        long tmp = recursivePow(digit,degree/2);
        return tmp*tmp;
    }
}

void reverse()
{

    char ch;
    std::cin >> ch;
    if (ch == '0')
    {
        return;
    }
    reverse();
    std::cout << ch << std::endl;
}




void printArray(int a[], int size){
    using namespace std;
    for (int i = 0; i < size; ++i) {
        cout << "array[" << i <<  "] = " << a[i] << endl;
    }
    cout << "_____________________________________\n" << endl;
}

void quickSort(int arr[], int left, int right) {
    int i = left, j = right;
    int tmp;

    int pivot = arr[left + (right - left) / 2];

    while (i <= j) {
        while (arr[i] < pivot) i++;
        while (arr[j] > pivot) j--;

        if (i <= j) {
            std::swap(arr[i], arr[j]);
            i++;
            j--;
        }
    };


    if (left < j) quickSort(arr, left, j);
    if (i < right) quickSort(arr, i, right);
}



int * getRandomArray(int size)
{

    srand(time(0));

    bool plus;

    int * result = new int[size];

    for (int i = 0; i < size; ++i) {

        plus = (rand() % 2 == 0);

        plus ? result[i] =  (rand() % 100) : result[i] = -(rand() % 100);
    }

    return result;
}



int main(int argc, char *args[])
{

    int * a = getRandomArray(SIZE);

    std::cout << "before soritng : \n" << std::endl;

    printArray(a,SIZE);

    quickSort(a, 0, SIZE - 1);

    std::cout << "after soritng : \n" << std::endl;

    printArray(a,SIZE);

    delete [] a;

    std::cout << recursivePow(11,11) << std::endl;

    return 0;
}