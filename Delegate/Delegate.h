//
// Created by Рамиль on 08.04.16.
//

#ifndef TEMPLATES_DELEGATE_H
#define TEMPLATES_DELEGATE_H

#include <iostream>
#include <unordered_set>

template<typename T1, typename T2>
class Delegate {

public:

    /*
     * Subscribe the method
     */
    void subscribe(void (*method)(T1, T2)) {
        ptrs.insert(method);
    }

    /*
     * Unsubscribe the method
     */
    void unsubscribe(void (*method)(T1, T2)) {
        ptrs.erase(method);
    }

    Delegate &operator+=(void (*method)(T1, T2)) {

        subscribe(method);

        return *this;
    }

    Delegate &operator-=(void (*method)(T1, T2)) {

        unsubscribe(method);

        return *this;
    }


    void operator()(T1 t1, T2 t2){
        for(auto f : ptrs){
            f(t1,t2);
        }
    }

private:

    //pointers to the methods
    std::unordered_set<void (*)(T1, T2)> ptrs;
};



template<typename... Params>
class VariadicDelegate {

public:

    /*
     * Subscribe the method
     */
    void subscribe(void (*method)(Params... params)) {
        ptrs.insert(method);
    }

    /*
     * Unsubscribe the method
     */
    void unsubscribe(void (*method)(Params...)) {
        ptrs.erase(method);
    }

    VariadicDelegate &operator+=(void (*method)(Params...)) {

        subscribe(method);

        return *this;
    }

    VariadicDelegate &operator-=(void (*method)(Params...)) {

        unsubscribe(method);

        return *this;
    }


    void operator()(Params ... params){

        for (auto f : ptrs){
            f(params...);
        }
    }

private:

    //pointers to the methods
    std::unordered_set<void (*)(Params...)> ptrs;
};

#endif //TEMPLATES_DELEGATE_H
