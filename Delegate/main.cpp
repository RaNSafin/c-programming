#include <iostream>
#include "Delegate.h"


void foo(double a, double b) {
    std::cout << "foo method " << a << " , " << b << std::endl;
}

void bar(double a, double b) {
    std::cout << "bar method " << a << " , " << b << std::endl;
}

void lol(double a, double b) {
    std::cout << "lol method " << a << " , " << b << std::endl;
}


void vFoo(int a, double b, char c) {
    std::cout << "vFoo method" << std::endl;
}

void vBar(int a, double b, char c) {
    std::cout << "vBar method" << std::endl;
}


int main() {

    Delegate<double, double> delegate;

    delegate += foo;
    delegate += bar;
    delegate += lol;

    delegate(1, 1);

    std::cout << "after removing bar method" << std::endl;
    delegate -= bar;

    delegate(2, 2);


    std::cout << "variadic delegate" << std::endl;

    VariadicDelegate<int, double, char> vDelegate;

    vDelegate += vFoo;
    vDelegate += vBar;

    vDelegate(-10, 10.2, 'a');

    std::cout << "after removing vBar method" << std::endl;

    vDelegate -= vBar;

    vDelegate(-10, 10.2, 'b');


    return 0;
}