#ifndef UNIQUE_PTR_TYRANT_PTR_H
#define UNIQUE_PTR_TYRANT_PTR_H

#include <iostream>

//default deleter
template<typename T>
struct tyrant_deleter {
    void operator()(T *&ptr) {
        delete (ptr);
        ptr = nullptr;
    }
};



template<typename T, typename Deleter = tyrant_deleter<T> >
class tyrant_ptr {

public:

    tyrant_ptr() {
        this->ptr = nullptr;
    }


    explicit tyrant_ptr(T *ptr)
            : tyrant_ptr() {

        std::swap(ptr, this->ptr);
    }


    T operator*() {
        return *ptr;
    }

    T *operator->() {
        return ptr;
    }

    T operator[](size_t index) {
        return ptr[index];
    }

    void reset(T *ptr) {
        //destroy this
        this->~tyrant_ptr();

        //if ptr is not nullptr create an instance in this
        if (ptr)
            new(this) tyrant_ptr(ptr);
        else
            this->ptr = nullptr;
    }

    T *release() {
        T *tmp = this->ptr;
        this->ptr = nullptr;
        return tmp;
    }

    void swap(tyrant_ptr<T, Deleter> &other) {
        std::swap(other.ptr, this->ptr);
    }


    tyrant_ptr &operator=(tyrant_ptr<T, Deleter> &&x) {
        reset(x.release());
        return *this;
    }

    explicit operator bool() const {
        return ptr != nullptr;
    }


    ~tyrant_ptr() {
        Deleter()(ptr);
    }


private:

    T *ptr;

    //prohibit copying
    tyrant_ptr(const tyrant_ptr &) = delete;

    tyrant_ptr &operator=(const tyrant_ptr &) = delete;
};

template<class T, class U>
tyrant_ptr<T> make_unique(U&& u)
{
   return tyrant_ptr<T>(new T(std::forward<U>(u)));
}

#endif //UNIQUE_PTR_TYRANT_PTR_H
