#include "tyrant_ptr.h"

template<typename T>
struct Deleter {
    void operator()(T *&ptr) {
        delete[] ptr;
    }
};


int main() {

    using namespace std;

    string * p = new string("string to be tested");

    string * p2 = new string("string to be tested 2");

    //create a tyrant_ptr inst.
    tyrant_ptr<string> t_ptr(p);

    cout << "t_ptr's value :\n";
    cout << *t_ptr << endl << endl;


    tyrant_ptr<string> t2_ptr(p2);
    cout << "t2_ptr's value :\n";
    cout << *t2_ptr << endl << endl;


    //assign t_ptr to t2_ptr throw std::move only, no copy
    t_ptr = std::move(t2_ptr);

    cout << "t_ptr pointer value after assignment to t2_ptr is : ";
    cout << *t_ptr << endl << endl;

    cout << "release() t_ptr method invocation " << endl;
    string *releasedStr = t_ptr.release();

    cout << "released pointer value is : ";
    cout << *releasedStr << endl;

    cout << "pointer value in t_ptr after releasing : ";

    if(t_ptr){ //operator bool()
        cout << *t_ptr << endl;
    } else{
        cout << "null" << endl << endl;
    }

    cout << "reset(releasedStr) method invocation " << endl;
    t_ptr.reset(releasedStr);
    cout << "t_ptr's value is : ";
    cout << *t_ptr << endl << endl;


    string *s = new string("str new");

    cout << "reset(s) method invocation " << endl;
    t_ptr.reset(s);
    cout << "t_ptr's value is : ";
    cout << *t_ptr << endl << endl;

    cout << "create q string and invoke t2_ptr.reset(q)" << endl;
    string *q = new string("q string");
    t2_ptr.reset(q);

    cout << "swap t_ptr with t2_ptr" << endl << endl;
    t_ptr.swap(t2_ptr);

    cout << "t_ptr = ";
    cout << *t_ptr << endl;
    cout << "t2_ptr = ";
    cout << *t2_ptr << endl;

    return 0;
}