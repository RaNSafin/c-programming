#include <iostream>
#include <assert.h>

using namespace std;

//Abstract factory

class Warrior {
public:
    virtual void attack() = 0;

    virtual void defend() = 0;

    virtual void sayName() {
        cout << "My name is " << name << endl;
    }

    virtual ~Warrior() { }

protected:
    string name;
};


class ElfWarrior
        : public Warrior {
public:

    ElfWarrior() { this->name = "Elf"; }

    void attack() {
        cout << "I shoot arrows! Give up, I'm an elf warrior! Arrghhh...!" << endl;
    }

    void defend() {
        cout << "I'm so quickly! You will never catch me!" << endl;
    }

};

class DwarfWarrior
        : public Warrior {
public:

    DwarfWarrior() {
        this->name = "Dwarf";
    }

    void attack() {
        cout << "I hit you! I'm a dwarf warrior! Rrrrrr...!" << endl;
    }

    void defend() {
        cout << "I'm so powerful! You will never win!" << endl;
    }

};


class WarriorAbstractFactory {
public:
    virtual Warrior *createWarrior() = 0;
};


class ElfWarriorFactory
        : public WarriorAbstractFactory {
public:
    Warrior *createWarrior() {
        return new ElfWarrior();
    }
};

class DwarfWarriorFactory
        : public WarriorAbstractFactory {
public:
    Warrior *createWarrior() {
        return new DwarfWarrior();
    }
};


//Strategy

class SortStrategy {
public:
    virtual void sort(int *array, size_t size) = 0;

    virtual ~SortStrategy() { }
};


class BubbleSortStrategy
        : public SortStrategy {

public:

    void sort(int *array, size_t size) {

        for (int i = 0; i < size; ++i) {

            for (int j = i + 1; j < size; ++j) {

                if (array[j] < array[i]) {
                    std::swap(array[i], array[j]);
                }
            }
        }
    }
};


class InsertionSortStrategy
        : public SortStrategy {

public:

    void sort(int *array, size_t size) {
        int i, j, tmp = 0;
        for (i = 1; i < size; i++) {
            j = i;
            while (j > 0 && array[j - 1] > array[j]) {
                tmp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = tmp;
                j--;
            }
        }
        return;
    }

};


class SelectionSortStrategy
        : public SortStrategy {

public:
    void sort(int *array, size_t size) {

        for (int i = 0; i < size - 1; i++) {
            int least = i;
            for (int j = i + 1; j < size - 1; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            std::swap(array[i], array[least]);
        }

    }
};


class BogoSortStrategy
        : public SortStrategy {

    bool correct(int *a, size_t size) {
        while (--size > 0)
            if (a[size - 1] > a[size])
                return false;
        return true;
    }

    void shuffle(int *a, size_t size) {
        for (int i = 0; i < size; i++) {

            std::swap(a[i], a[(rand() % size)]);
        }
    }

    void sort(int *array, size_t size) {
        while (!correct(array, size))
            shuffle(array, size);
    }
};


class Sort {

private:
    SortStrategy *algo;
public:

    Sort() {
    }

    Sort(SortStrategy *algo) {
        if (algo) {
            this->algo = algo;
        }
    }

    void setAlgo(SortStrategy *algo) {
        assert(algo != nullptr);
        this->algo = algo;
    }

    void executeSort(int *array, size_t size) {
        if (!algo) {
            cerr << "algo wasn't chosen" << endl;
            return;
        }
        algo->sort(array, size);
    }

    ~Sort() { }

};


class A {
public:
    virtual void print() = 0;

    virtual  ~A() { }
};


class B
        : public A {

public:
    void print() {
        cout << "Hello I am B" << endl;
    }
};

class C
        : public A {
public:
    void print() {
        cout << "Hello I am C" << endl;
    }
};


bool method(A *obj1, A *obj2) {
    long *type1 = (long *) obj1;
    long *type2 = (long *) obj2;

    return type1[0] == type2[0];
}


int main() {


    C c;
    B b;
    C c2;


    cout << "Is c2 has the same class as the b? : " << (method(&c2, &b) ? "true" : "false") << endl;
    cout << "Is c2 has the same class as the c? : " << (method(&c2, &c) ? "true" : "false") << endl;

    cout << "===============================" << endl;


    //AbstractFactory

    ElfWarriorFactory elfWarriorFactory;
    DwarfWarriorFactory dwarfWarriorFactory;


    Warrior *warrior = elfWarriorFactory.createWarrior();
    Warrior *warrior1 = dwarfWarriorFactory.createWarrior();

    warrior->attack();
    warrior->defend();
    warrior->sayName();

    cout << "============================" << endl;

    warrior1->attack();
    warrior1->defend();
    warrior1->sayName();

    delete (warrior);
    delete (warrior1);

    cout << endl;

    //Strategy pattern

    cout << "Enter array size" << endl;
    size_t arrSize = 0;
    cin >> arrSize;

    int *array = new int[arrSize];

    cout << "Enter array elements" << endl;
    for (int j = 0; j < arrSize; ++j) {
        cin >> array[j];
    }


    SortStrategy *strategy = new BogoSortStrategy();

    Sort *sort = new Sort(strategy);
    sort->executeSort(array, arrSize);


    for (int i = 0; i < arrSize; ++i) {
        if (i < arrSize - 1) {
            cout << array[i] << ", ";
        } else {
            cout << array[i] << endl;
        }
    }

    delete sort;
    delete strategy;

    delete[] array;

    return 0;
}