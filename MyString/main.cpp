#include<cstring>
#include<algorithm>
#include<iostream>

using namespace std;


class String {
public:

    String() {
        size = 0;
        data = nullptr;
    }

    String(const char *str) {
        size = strlen(str);
        data = new char[size + 1];
        memcpy(data, str, size + 1);
    }

    explicit String(const String &s) {
        size = s.size;
        data = new char[size + 1];
        memcpy(data, s.data, size + 1);
    }

    explicit String(String &&s) : String() {
        swap(data, s.data);
        swap(size, s.size);
    }


    String &operator=(const String &s) {
        this->~String();
        new(this) String(s);
        return *this;
    }


    char &operator[](size_t idx) {
        return data[idx];
    }

    int compareTo(const String &other) const {
        return strcmp(this->data, other.data);
    }


    int compareTo(const char *other) const {
        return strcmp(this->data, other);
    }


    String &operator+=(const String &other) {
        char *newData = new char[size + other.size + 1];
        memcpy(newData, data, size);
        memcpy(newData + size, other.data, other.size + 1);
        delete[]data;
        data = newData;
        size += other.size;
        return *this;
    }


    String &toLowerCase() {
        for (int i = 0; i < this->size; ++i) {
            this->data[i] = (char) tolower(this->data[i]);
        }
        return *this;
    }


    size_t length() const {
        return this->size;
    }

    char *cString() const {
        return this->data;
    }

    ~String() {
        delete[]data;
    }


private:
    char *data;
    size_t size;
};


/**
 * Stream operators
 */
std::ostream &operator<<(ostream &os, const String &str) {
    os << str.cString();
    return os;
}

std::istream &operator>>(istream &is, String &str) {
    is >> str.cString();
    return is;
}

/**
 * compare operators
 */

bool operator==(const String &left, const String &right) {
    return left.compareTo(right) == 0;
}

bool operator>(const String &left, const String &right) {
    return left.compareTo(right) > 0;
}

bool operator<(const String &left, const String &right) {
    return left.compareTo(right) < 0;
}

bool operator<=(const String &left, const String &right) {
    return !operator>(left, right);
}

bool operator>=(const String &left, const String &right) {
    return !operator>(left, right);
}


bool operator==(const String &left, const char *right) {
    return left.compareTo(right) == 0;
}

bool operator>(const String &left, const char *right) {
    return left.compareTo(right) > 0;
}

bool operator<(const String &left, const char *right) {
    return left.compareTo(right) < 0;
}

bool operator<=(const String &left, const char *right) {
    return !operator>(left, right);
}

bool operator>=(const String &left, const char *right) {
    return !operator>(left, right);
}


/**
 * addition operators
 */
String operator+(const String &str1, const String &str2) {
    String tmp(str1);
    tmp += str2;
    return move(tmp);
}

String operator+(String &&str1, const String &str2) {
    str1 += str2;
    return move(str1);
}

String operator+(const String &str1, String &&str2) {
    str2 += str1;
    return move(str2);
}

String operator+(String &&str1, String &&str2) {
    str1 += str2;
    return move(str1);
}


int main() {

    return 0;
}
