#include<iostream>
#include <list>

using namespace std;


//forward declaration of iterator
template<typename T>
class List_Iterator;


//bi-directional list
template<typename T>
class List {
public:

    friend class List_Iterator<T>;

    typedef List_Iterator<T> iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;


    List() : head(nullptr), tail(nullptr), size(0) { }

    ~List() {

        Node *tmp;
        while (head) {
            tmp = head;
            head = head->next;
            delete tmp;
        }
    }


    bool isEmpty() const {
        return head == nullptr;
    }


    void push_back(const T &elem) {
        Node *node = new Node(elem, nullptr, tail);
        switch (size) {
            case 0 :
                head = node;
                break;
            case 1 :
                head->next = node;
                break;
            default :
                tail->next = node;
        }
        tail = node;
        ++size;
    }

    void push_front(const T &elem) {
        Node *node = new Node(elem, head);
        switch (size) {
            case 0 :
                tail = node;
                break;
            case 1 :
                tail->prev = node;
                break;
            default :
                head->prev = node;
        }
        head = node;
        ++size;
    }

    //iterators
    iterator begin() {
        return iterator(head);
    }

    iterator end() {
        return iterator(nullptr, tail);
    }

    reverse_iterator rbegin() {
        return reverse_iterator(end());
    }

    reverse_iterator rend() {
        return reverse_iterator(begin());
    }

private:

    struct Node {
        Node(const T &elem, Node *next = nullptr, Node *prev = nullptr)
                : elem(elem), next(next), prev(prev) { }

        T elem;
        Node *next;
        Node *prev;
    };

    Node *head, *tail;
    size_t size;
};


template<typename T>
class List_Iterator {
public:

    typedef std::ptrdiff_t difference_type;
    typedef std::bidirectional_iterator_tag iterator_category;
    typedef T value_type;
    typedef T *pointer;
    typedef T &reference;

    reference operator*() const {
        return pointee->elem;
    }

    pointer operator->() const { return &(operator*()); }

    List_Iterator &operator++() {
        if (pointee) {
            prevPointee = pointee;
            pointee = pointee->next;
        }
        return *this;
    }

    List_Iterator &operator--() {
        if (prevPointee) {
            pointee = prevPointee;
            prevPointee = prevPointee->prev;
        }
        return *this;
    }

    bool operator==(const List_Iterator &other) const {
        return this->pointee == other.pointee;
    }

    bool operator!=(const List_Iterator &other) const { return !operator==(other); }

    List_Iterator(typename List<T>::Node *pointee = nullptr, typename List<T>::Node *prevPointee = nullptr)
            : pointee(pointee), prevPointee(prevPointee) { }

private:
    typename List<T>::Node *pointee;
    typename List<T>::Node *prevPointee;
};


int main() {

    list<int> l;

    List<string> sList;

    sList.push_front("a");
    sList.push_front("c");
    sList.push_front("b");
    sList.push_back("w");
    sList.push_back("k");

    cout << "values in list :" << endl;
    for (auto iter = sList.begin(); iter != sList.end(); ++iter) {
        cout << *iter << " - ";
    }
    cout << endl;

    cout << "reverse()" << endl;
    std::reverse(sList.begin(), sList.end());

    cout << "after reversing : " << endl;
    for (auto iter = sList.begin(); iter != sList.end(); ++iter) {
        cout << *iter << " - ";
    }
    cout << endl;
    return 0;
}