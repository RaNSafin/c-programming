#ifndef SHARED_PTR_SHARED_PTR_H
#define SHARED_PTR_SHARED_PTR_H

#include <cstddef>
#include <memory>
#include <cassert>
#include <iostream>

using namespace std;

template<typename T>
struct default_deleter {
    void operator()(T *&ptr) {
        delete ptr;
        ptr = nullptr;
    }
};


template<typename T, typename Deleter = default_deleter<T> >
struct sharedPtr {

    sharedPtr() : ptr(nullptr), control(nullptr) {
    }

    sharedPtr(std::nullptr_t) : sharedPtr() { }

    //simple constructor when U is T
    template<typename U=T>
    sharedPtr(U *ptr) :
            ptr(ptr), control(new auxImpl<U>(ptr)) {
    }

    sharedPtr(const sharedPtr &other) {
        acquire(other);
        ptr = other.ptr;

    }

    template<typename U>
    sharedPtr(const sharedPtr<T> &other, U *part) {
        acquire(other);
        ptr = part;
    }

    sharedPtr(unique_ptr<T> &&x) : sharedPtr(x.release()) { }

    ~sharedPtr() {
        release();
    }

    T operator*() const {
        return *ptr;
    }

    T *get() const {
        return ptr;
    }

    size_t use_count() const {
        return control ? control->count : 0;
    }

    T *operator->() {
        return get();
    }

    void swap(sharedPtr &other) {
        if (other.control != control) {
            std::swap(ptr, other.ptr);
            std::swap(control, other.control);
        }
    }


    sharedPtr &operator=(const sharedPtr<T> &other) {
        if (other.control && this->control != other.control) {
            this->~sharedPtr();
            new(this) sharedPtr(other);
        }
        return *this;
    }

    sharedPtr &operator=(sharedPtr<T> &&other) {
        this->~sharedPtr();
        swap(other);
        return *this;
    }

    sharedPtr &operator=(unique_ptr<T> &&x) {
        this->~sharedPtr();
        new(this) sharedPtr(std::move(x));
        return *this;
    }

    bool isEmpty() const {
        return ptr == nullptr;
    }

    bool unique() const {
        if (control)
            return control->count == 1;
        return false;
    }

    void reset() {
        this->~sharedPtr();
        new(this) sharedPtr();
    }

    void reset(T *p) {
        this->~sharedPtr();
        new(this) sharedPtr(p);
    }

private:

    struct aux {
        explicit aux() : count(1), weak_count(0) { }

        virtual void destroy() = 0;

        virtual ~aux() { };

        size_t count;
        size_t weak_count;
    };


    template<typename U=T>
    struct auxImpl : aux {
        U *ptr;

        auxImpl(U *ptr = nullptr) : aux(), ptr(ptr) { };

        virtual void destroy() {
            Deleter()(ptr);
        }

        virtual ~auxImpl() {
            destroy();
        }
    };

    template<typename U=T>
    void acquire(const sharedPtr<U> &other) {
        if (other.control) {
            control = other.control;
            ++control->count;
        }
    }

    void release() {
        if (control && --control->count == 0) {
            delete control;
            control = nullptr;
        }
    }

public:
    T *ptr;
    aux *control;


};

template<typename T, typename... Args>
sharedPtr<T> makeShared(Args &&... args) {
    return sharedPtr<T>(new T(std::forward<Args>(args)...));
}


#endif //SHARED_PTR_SHARED_PTR_H
