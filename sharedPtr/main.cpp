#include "sharedPtr.h"

using namespace std;

struct Foo {

    Foo(int i) {
        a = new int(i);
    }

    ~Foo() {
        delete[] a;
    }

    int *getInt() const {
        return a;
    }

private:
    int *a;
};

struct foo_deleter {

    void operator()(Foo *&ptr) {
        delete ptr;
        ptr = nullptr;
    }
};

void testFoo(const sharedPtr<Foo> &ptr, string name) {
    cout << "----- " << name << " -----" << endl;
    cout << "\t" << "use_count is : " << ptr.use_count() << endl;
    cout << "\t" << "isEmpty method : " << (ptr.isEmpty() ? "true" : "false") << endl;
    cout << "\t" << "unique method : " << (ptr.unique() ? "true" : "false") << endl;
    if (!ptr.isEmpty()) {
        cout << "\t" << "pointers mean : " << *(ptr.get()->getInt()) << endl;
    } else {
        cout << "\t" << "pointer is null" << endl;
    }
    cout << endl;
};

void test(const sharedPtr<int> &ptr, string name) {

    cout << "----- " << name << " -----" << endl;
    cout << "\t" << "use_count is : " << ptr.use_count() << endl;
    cout << "\t" << "isEmpty method : " << (ptr.isEmpty() ? "true" : "false") << endl;
    cout << "\t" << "unique method : " << (ptr.unique() ? "true" : "false") << endl;
    if (!ptr.isEmpty()) {
        cout << "\t" << "pointers mean : " << *ptr << endl;
    } else {
        cout << "\t" << "pointer is null" << endl;
    }
    cout << endl;
}

const string delimiter = "==========================================";

int main() {

//    /*
//==============TEST==============

    cout << "creating p ()" << endl;
    sharedPtr<int> p; //empty pointer
    test(p, "p");

    cout << delimiter << endl;

    int *i = new int(10);

    cout << "creating p1 (i)" << endl;
    sharedPtr<int> p1(i);
    test(p1, "p1");

    cout << delimiter << endl;

    cout << "creating p2 (p)" << endl;
    sharedPtr<int> p2(p);
    test(p2, "p2");

    cout << delimiter << endl;

    cout << "assigning p2 = p1" << endl;
    p2 = p1;
    test(p1, "p1");
    test(p2, "p2");

    cout << delimiter << endl;

    cout << "creating p3 ( &&p1 )" << endl;
    sharedPtr<int> p3(std::move(p1));

    test(p1, "p1");
    test(p2, "p2");
    test(p3, "p3");

    cout << delimiter << endl;

    cout << "creating p4 (p3)" << endl;
    sharedPtr<int> p4(p3);
    test(p4, "p4");
    test(p3, "p3");

    cout << delimiter << endl;

    cout << "invoking p4.reset(new int(33))" << endl;
    p4.reset(new int(33));
    test(p4, "p4");
    test(p3, "p3");

    cout << delimiter << endl;

    cout << "invoking p3.reset()" << endl;
    p3.reset();
    test(p3, "p3");

    cout << delimiter << endl;

    cout << "creating p5,  p5 = makeShared(12) " << endl;
    sharedPtr<int> p5 = makeShared<int>(12);
    test(p5, "p5");

    cout << delimiter << endl;

    cout << "assigning p5 = std::move(p4)" << endl;
    p5 = std::move(p4);
    test(p5, "p5");
    test(p4, "p4");

    cout << delimiter << endl;


    cout << "assigning p6 = p5" << endl;
    sharedPtr<int> p6;
    p6 = p5;
    test(p5, "p5");
    test(p6, "p6");

    cout << delimiter << endl;

    cout << "p4 and p6 before swap invocation :" << endl;
    test(p4, "p4");
    test(p6, "p6");
    cout << "after swap :" << endl;
    p4.swap(p6);
    test(p4, "p4");
    test(p6, "p6");

    cout << delimiter << endl;

    cout << "creating unique_ptr<int> unique (new int(55)) " << endl;
    std::unique_ptr<int> unique(new int(55));
    cout << "it's value is : " << *unique << endl << endl;

    cout << "assigning p4 = std::move(unique)" << endl;
    p4 = std::move(unique);


    cout << "unique value's address is : " << unique.get() << endl << endl;
    test(p4, "p4");


    cout << delimiter << endl;

    cout << "reseting unique, unique.reset(new int (66)))" << endl << endl;
    unique.reset(new int(66));


    cout << "unique's value is " << *unique << endl;


    cout << "creating p7, invoking sharedPtr(std::move(unique))" << endl << endl;
    sharedPtr<int> p7(std::move(unique));

    cout << "unique_ptr's value is " << unique.get() << endl << endl;
    test(p7, "p7");

    cout << delimiter << endl;


    cout << "creating foo_ptr makeShared(int arg)" << endl;
    sharedPtr<Foo> foo_ptr = makeShared<Foo>(222);

    testFoo(foo_ptr, "foo_ptr");

    cout << delimiter << endl;

    sharedPtr<int> scope1;
    cout << "before scope scope1 sharedPtr : " << endl;
    test(scope1, "scope1");

    {
        scope1 = makeShared<int>(1111);
        sharedPtr<int> scope2(scope1);
        sharedPtr<int> scope3(scope1);
        sharedPtr<int> scope4(scope2);
        sharedPtr<int> scope5(scope4);

        cout << "in scope were created 4 sharedPtrs" << endl;
        cout << "in scope scope1 sharedPtr" << endl;
        test(scope1, "s1");
    }

    cout << "after scope : " << endl;
    test(scope1, "s1");


    sharedPtr<Foo> obj(new Foo(11));
    {
        sharedPtr<Foo> p9(obj, obj.get());
    }

    cout << obj.get() << endl;


//=============END TEST===========

}
