#ifndef MyVector_HEADER
#define MyVector_HEADER

#include <stddef.h>
#include <string.h>
#include <iostream>

struct MyVector
{

    private:

        const size_t DEFAULT_CAPACITY = 16;

        int * arr;

        size_t capacity;    //the capacity of vector
        size_t count;       //current filled  size of vector


        int* create(size_t size)
        {
            int * arr = new int[size];
            for (int i = 0; i < size; ++i) {
                arr[i] = 0;
            }
            return arr;
        }


        void resize(int* &arr, size_t& oldSize, size_t newSize)
        {
            if (arr && oldSize != newSize)
            {
                int *newArr = create(newSize);
                memcpy(newArr,arr, sizeof(int) * ( (oldSize < newSize) ? oldSize : newSize) );
                delete[] arr;
                arr = newArr;
                oldSize = newSize;
            }
        }


        void clear(int* &arr, size_t& capacity, size_t& count)
        {
            delete [] arr;
            arr = 0;
            capacity = 0;
            count = 0;
        }


    public:

        MyVector()
        {
            arr = create(DEFAULT_CAPACITY);
            count = 0;
            capacity = DEFAULT_CAPACITY;
        }


        ~MyVector()
        {
            clear(arr,capacity,count);
        }


        int get(size_t index){

            if (index < count)
            {
                return arr[index];
            }

            //throw an exception
            return -1;
        }


        void set(size_t index, int value)
        {
            if (index < count){

                arr[index] = value;

            } else {
                //throw an exception
            }
        }



        void add(int value)
        {
            if (count < capacity)
            {

                arr[count++] = value;

            } else {

                resize(arr,capacity,capacity+DEFAULT_CAPACITY);

                arr[count++] = value;
            }
        }



        void insert(size_t index, int value)
        {
            if(index < count)
            {
                if (count >= capacity)
                {
                    resize(arr,capacity,capacity+DEFAULT_CAPACITY);
                }


                //переносим все данные начиная с arr[index] вправо
                for (int i = count-1; i >= index  && i >= 0; --i) {
                    arr[i+1] = arr[i];
                }
                arr[index] = value;
            }
            //throw an exception
        }



        void fit()
        {
            if (count < capacity)
            {
                resize(arr,capacity,count);
            }
        }


        void print(){

            for (int i = 0; i < capacity; ++i)
            {
                std::cout << arr[i] << "  ";
            }

            std::cout << std::endl;
        }


        void clear()
        {
            capacity = DEFAULT_CAPACITY;
            count = 0;

            arr = create(capacity);
        }


        size_t getSize()
        {
            return count;
        }


};

#endif