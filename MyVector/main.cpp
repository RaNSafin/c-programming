#include <iostream>
#include "MyVector.h"

using namespace std;


int main()
{
    MyVector *vector = new MyVector();

    vector->add(1);
    vector->add(2);
    vector->add(3);

    cout << "get(0) = " << vector->get(0) << endl;
    cout << "get(1) = " << vector->get(1) << endl;

    vector->set(0, -1);
    cout << "get(0) = " << vector->get(0) << endl;

    vector->print();

    vector->fit();

    vector->add(11);

   	vector->print();

   	vector->clear();

   	vector->print();

    delete vector;

    return 0;
}
