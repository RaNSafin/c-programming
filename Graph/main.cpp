#include <fstream>
#include <iostream>
#include <vector>
#include <queue>
#include <cassert>

using namespace std;

/**
 * DSU - Disjoint - Set - Unite structure
 */
struct DSU {

private:

    vector<size_t> parent;
    vector<size_t> rank;

public:

    //constructor with count of elements in set
    DSU(size_t size) {
        parent.reserve(size);
        rank.reserve(size);
    }

    //makes a single element subset
    void makeSet(size_t x) {
        parent[x] = x;
        rank[x] = 0;
    }

    //finds the parent of x
    size_t findSet(size_t x) {
        return (x == parent[x]) ? x : (parent[x] = findSet(parent[x]));
    }

    //unite two sets into one
    void uniteSets(size_t x, size_t y) {
        x = findSet(x); //find the parent of x
        y = findSet(y); //find the parent of y
        if (x != y) {
            if (rank[x] < rank[y]) {
                std::swap(x, y);
            }
            parent[y] = x;
            if (rank[x] == rank[y]) {
                rank[x]++;
            }
        }
    }
};


struct Graph {

private:

    vector<vector<pair<size_t, double> > > edgeLists;

    void addEdge(size_t from, size_t to, double weight) {
        edgeLists[from].push_back(pair<size_t, double>(to, weight));
    }


    /**
     * Useful method to convert our graph structure to convenient sorted priority queue
     * where stores pairs (of edge weight and pair of edge coordinates)
     */

    priority_queue<pair<double, pair<size_t, size_t> >,
            vector<pair<double, pair<size_t, size_t> > >,
            greater<pair<double, pair<size_t, size_t> > > > getEdges() {

        //result
        priority_queue<pair<double, pair<size_t, size_t> >, vector<pair<double, pair<size_t, size_t> > >,
                greater<pair<double, pair<size_t, size_t> > > > res;

        for (size_t i = 0; i < edgeLists.size(); ++i) {

            for (size_t j = 0; j < edgeLists[i].size(); ++j) {
                res.push(make_pair(edgeLists[i][j].second, make_pair(i, edgeLists[i][j].first)));
            }
        }
        return res;
    }

public:

    /**
     * Constructor that makes an adjacent list from input file
     */

    Graph(const std::string filename) {

        ifstream fin(filename);

        assert(!fin.fail());

        size_t vertexCount;
        fin >> vertexCount;

        edgeLists.reserve(vertexCount);

        for (size_t i = 0; i < vertexCount; i++) {
            edgeLists.push_back({});    //an empty vector
        }

        for (size_t i = 0; i < vertexCount; i++) {
            for (size_t j = 0; j < vertexCount; j++) {

                double weight;
                fin >> weight;

                if (weight > 0) {
                    addEdge(i, j, weight);
                }
            }
        }

        fin.close();
    }

    /**
     * Algorithm Dijkstra
     * Returns the minimum way's cost
     */

    //Should use a wrapper with some bool isInfinity
    const unsigned long long INF = 1000000000000000000;

    long double dijkstra(size_t from, size_t to) { 

        vector<double> d(edgeLists.size(), INF);    //init with INF values

        //starting vertex's cost to itself
        d[from] = 0;

        //priority queue to store pair's in sorted state
        priority_queue<pair<double, size_t>, vector<pair<double, size_t> >, greater<pair<double, size_t> > > queue;

        queue.push(make_pair(0.0, from));   // from vertex pair

        while (!queue.empty()) {

            size_t currentVertex = queue.top().second;
            double currentDest = queue.top().first;

            queue.pop();

            if (currentDest > d[currentVertex]) {
                continue;
            }

            for (size_t i = 0; i < edgeLists[currentVertex].size(); ++i) {

                size_t dest = edgeLists[currentVertex][i].first;
                double length = edgeLists[currentVertex][i].second;

                if ((d[currentVertex] + length < d[dest])) {
                    d[dest] = d[currentVertex] + length;
                    queue.push(make_pair(d[dest], dest));
                }
            }
        }
        return d[to];
    }


    /**
     * Kruskal algorithm with DSU 
     */

    vector<pair<size_t, size_t> > kruskal() {

        DSU dsu(edgeLists.size());    //DSU structure

        //DSU structure initialization
        for (size_t j = 0; j < edgeLists.size(); ++j) {
            //making subsets
            dsu.makeSet(j);
        }

        //result pairs of edges that form minimal spanning tree
        vector<pair<size_t, size_t> > minSpanningTree;

        auto edgesList = getEdges();    //stores pairs (weight and pair of vertexes)
        double resultWayCost = 0.0;     //result cost of minimal spanning tree

        while (!edgesList.empty()) {

            double weight = edgesList.top().first;      //weight
            size_t x = edgesList.top().second.first;    //from
            size_t y = edgesList.top().second.second;   //to

            if (dsu.findSet(x) != dsu.findSet(y)) {
                resultWayCost += weight;
                minSpanningTree.push_back(edgesList.top().second);
                dsu.uniteSets(x, y);
            }

            edgesList.pop();
        }

        cout << "minimal spanning tree weight : " << resultWayCost << endl;

        return minSpanningTree;
    };


    friend ostream &operator<<(ostream &out, Graph g);
};


ostream &operator<<(ostream &out, Graph g) {
    size_t vertexCount = g.edgeLists.size();
    out << vertexCount << '\n';
    double *weights = new double[vertexCount];
    for (size_t i = 0; i < vertexCount; i++) {
        for (size_t j = 0; j < vertexCount; j++) {
            weights[j] = -1;
        }
        for (auto it = g.edgeLists[i].begin(); it != g.edgeLists[i].end(); ++it) {
            weights[it->first] = it->second;
        }
        for (size_t j = 0; j < vertexCount; j++) {
            out << weights[j] << ' ';
        }
        out << '\n';
    }
    delete[]weights;
    return out;
}


int main(int argc, char *argv[]) {

    if (argc < 2) {
        std::cerr << "You should write down at least one argument" << std::endl;
        return -1;
    }

    Graph g(argv[1]);

    std::cout << "adjacent list : " << std::endl;
    std::cout << g << std::endl;


    size_t from;
    size_t to;

    std::cout << "Enter 'from' vertex index" << std::endl;
    std::cin >> from;

    std::cout << "Enter 'to' vertex index" << std::endl;
    std::cin >> to;

    std::cout << "minimal path cost from " << from << " to " << to << " : "
    << g.dijkstra(from - 1, to - 1) << std::endl << std::endl;
    auto res = g.kruskal();


    std::cout << "edges in minimal spanning tree : " << std::endl;
    for (int i = 0; i < res.size(); ++i) {
        size_t v1 = res[i].first;
        size_t v2 = res[i].second;
        std::cout << "( " << v1 + 1 << ", " << v2 + 1 << " )" << std::endl;
    }

    return 0;
}