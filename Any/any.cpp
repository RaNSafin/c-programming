#include <iostream>

using namespace std;

class any {
public:

    any() : content(nullptr) { }

    any(const any &other) : content(other.content ? other.content->clone() : nullptr) { }

    any(any &other) : content(other.content ? other.content->clone() : nullptr) { }

    any(any &&other) : content(other.content ? other.content->clone() : nullptr) { }


    template<typename ValueType>
    any(ValueType &&val) : content(new holder<ValueType>(forward<ValueType>(val))) { }

    ~any() {
        delete content;
        content = nullptr;
    }

    any &swap(any &other) {
        std::swap(content, other.content);
        return *this;
    }

    any &operator=(const any &other) {
        any(other).swap(*this);
        return *this;
    }

    any &operator=(any &&other) {
        any(other).swap(*this);
        return *this;
    }

    template<typename ValueType>
    any &operator=(const ValueType &val) {
        any(val).swap(*this);
        return *this;
    }

    template<typename ValueType>
    any &operator=(ValueType &&val) {
        any(move(val)).swap(*this);
        return *this;
    }

    bool empty() const {
        return content == nullptr;
    }

    const std::type_info &type() const {
        return content == nullptr ? typeid(void *) : content->type_info();
    }

private:
    class placeholder {
    public:
        virtual ~placeholder() { }

        virtual const std::type_info &type_info() const = 0;

        virtual placeholder *clone() const = 0;
    };

    template<typename ValueType>
    class holder
            : public placeholder {
    public:
        holder(const ValueType &value)
                : held(value) { }

        holder(ValueType &&value)
                : held(forward<ValueType>(value)) {

        }

        virtual const std::type_info &type_info() const {
            return typeid(ValueType);
        }

        virtual placeholder *clone() const {
            return new holder(held);
        }

        const ValueType held;
    };

    placeholder *content;

    template<typename T>
    T any_cast()const {
        holder<T> *resultHolder = static_cast<holder<T> *>(content);
        if (resultHolder)
            return resultHolder->held;
        else
            throw std::bad_cast();
    }

    template<typename T>
    friend T any_cast(any &a);

    template<typename T>
    friend T any_cast(const any &a);

    template<typename ValueType>
    friend const ValueType *any_cast(const any *pa);

    template<typename ValueType>
    friend ValueType *any_cast(any *pa);
};

template<typename T>
T any_cast(any &a) {
    return a.any_cast<T>();
}

template<typename T>
T any_cast(const any &a) {
    return a.any_cast<T>();
}


template<typename ValueType>
const ValueType *any_cast(const any *pa) {
    if (!pa) return nullptr;
    any::holder<ValueType> *resultHolder = dynamic_cast<any::holder<ValueType> *>(pa->content);
    if (resultHolder)
        return (&resultHolder->held);
    return nullptr;
}


template<typename ValueType>
ValueType *any_cast(any *pa) {

    if (!pa) return nullptr;

    any::holder<ValueType> *resultHolder = dynamic_cast<any::holder<ValueType> *>(pa->content);
    if (resultHolder)
        return const_cast<ValueType *>(&resultHolder->held);
    return nullptr;
};

int main() {

    any i = 5;
    any d = 5.0;
    any s = string("5");


    try {

        double d_val = any_cast<double>(d);
        int i_val = any_cast<int>(d);
        string s_val = any_cast<string>(s);

        cout << i_val << endl << d_val << endl << s_val << endl;
    }
    catch (bad_cast b) {
        cout << "Bad cast" << endl;
    }

    any *newAnyInt = new any(12);
    int * value = any_cast<int>(newAnyInt);
    cout << *value << endl;

    return 0;
}