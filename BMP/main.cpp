#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

#pragma pack(push,2)

struct BITMAPFILEHEADER {

    uint16_t bfType;
    uint32_t bfSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;

};


struct BITMAPINFO {

    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    int32_t biXPelsPerMeter;
    int32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


struct RGB {
    uint8_t blue;
    uint8_t green;
    uint8_t red;

    RGB(uint8_t blue, uint8_t green, uint8_t red) {
        this->red = red;
        this->blue = blue;
        this->green = green;
    }

    RGB() { }
};


struct PointXY {

    size_t x;
    size_t y;

    PointXY(size_t x, size_t y) : x(x), y(y) { }
};


/**
 * Loads BMP file and returns RGB array
 * bh and bi structures are saved
 */

RGB **loadBMP(const std::string filePath, BITMAPFILEHEADER &bh, BITMAPINFO &bi) {

    std::fstream fin(filePath, std::ios_base::binary | std::ios_base::in);

    assert(!fin.fail());

    fin.read((char *) &bh, 14);
    fin.read((char *) &bi, 40);

    RGB rgb;
    int padding;

    padding = (bi.biWidth * 3) % 4 ? 4 - ((bi.biWidth * 3) % 4) : 0;

    RGB **rgbGrid = new RGB *[bi.biHeight];
    for (int i = 0; i < bi.biHeight; ++i) {
        rgbGrid[i] = new RGB[bi.biWidth];
    }

    for (int i = 0; i < bi.biHeight; ++i) {
        for (int j = 0; j < bi.biWidth; ++j) {

            fin.read((char *) &(rgbGrid[i][j]), 3);
        }

        if (padding > 0) {
            //reading remain if exists
            fin.read((char *) &rgb, padding);
        }
    }

    fin.close();

    return rgbGrid;
}


/**
 * Writes BMP file into output
 */

void writeBMP(const std::string outputPath, BITMAPFILEHEADER bh, BITMAPINFO bi, RGB **rgbGrid) {

    std::fstream fout(outputPath, std::ios_base::binary | std::ios_base::out);

    assert(!fout.fail());

    fout.write((char *) &bh, 14);
    fout.write((char *) &bi, 40);

    int padding = (bi.biWidth * 3) % 4 ? 4 - ((bi.biWidth * 3) % 4) : 0;

    for (int i = 0; i < bi.biHeight; ++i) {

        for (int j = 0; j < bi.biWidth; ++j) {
            fout.write((char *) &rgbGrid[i][j], 3);
        }

        if (padding > 0) {
            fout.write((char *) &padding, padding);
        }
    }

    fout.close();
}


/**
 * Inverts pixel
 */

void invertPixel(RGB *rgb) {
//    RGB *r = new RGB;
    rgb->red = (uint8_t) (255 - rgb->red);
    rgb->green = (uint8_t) (255 - rgb->green);
    rgb->blue = (uint8_t) (255 - rgb->blue);
//    return r;
}


/**
 * Converts into reverse values
 */

void invert(const std::string outputPath, BITMAPFILEHEADER bh, BITMAPINFO bi, RGB **rgbGrid) {

    std::fstream fout(outputPath, std::ios_base::binary | std::ios_base::out);

    assert(!fout.fail());

    fout.write((char *) &bh, 14);
    fout.write((char *) &bi, 40);

    int padding = (bi.biWidth * 3) % 4 ? 4 - ((bi.biWidth * 3) % 4) : 0;

    for (int i = 0; i < bi.biHeight; ++i) {
        for (int j = 0; j < bi.biWidth; ++j) {
            invertPixel(&rgbGrid[i][j]);
            fout.write((char *) &rgbGrid[i][j], 3);
        }
        if (padding > 0) {
            fout.write((const char *) &padding, padding);
        }
    }

    fout.close();
}


void clear(RGB **a, int h) {
    for (int i = 0; i < h; ++i) {
        delete [] a[i];
    }

    delete [] a;
}


/**
 * Xor two RGB
 */

void xorRGB(RGB one, RGB two, RGB &res) {
    res.blue = one.blue ^ two.blue;
    res.green = one.green ^ two.green;
    res.red = one.red ^ two.red;
}

/**
 * Xor two RGB files into output file
 */

void xorBMP(const std::string file1, const std::string file2, const std::string output) {

    BITMAPFILEHEADER bh1;
    BITMAPINFO bi1;

    RGB **grid1 = loadBMP(file1, bh1, bi1);

    BITMAPFILEHEADER bh2;
    BITMAPINFO bi2;
    RGB **grid2 = loadBMP(file2, bh2, bi2);

    if (bi1.biHeight != bi2.biHeight || bi1.biWidth != bi2.biWidth) {
        clear(grid1, bi1.biHeight);
        clear(grid2, bi2.biHeight);
        std::cerr << "pictures are not compatible" << std::endl;
        return;
    }


    std::fstream fout(output, std::ios::binary | std::ios::out);

    if(fout.fail()){
        clear(grid1, bi1.biHeight);
        clear(grid2, bi2.biHeight);
        return;
    }

    fout.write((char *) &bh1, 14);
    fout.write((char *) &bi1, 40);

    int padding = (bi1.biWidth * 3) % 4 ? 4 - ((bi1.biWidth * 3) % 4) : 0;

    for (int i = 0; i < bi1.biHeight; ++i) {
        for (int j = 0; j < bi1.biWidth; ++j) {
            RGB res;
            xorRGB(grid1[i][j], grid2[i][j],res);
            fout.write((char *)&res, 3);
        }
        if (padding > 0) {
            fout.write((const char *) &padding, padding);
        }
    }

    fout.close();

    clear(grid1, bi1.biHeight);
    clear(grid2, bi2.biHeight);

}


/**
 * Checks if colorOne equals to colorTwo
 */

bool RGB_equals(RGB colorOne, RGB colorTwo) {

    return (colorOne.green == colorTwo.green &&
            colorOne.blue == colorTwo.blue && colorOne.red == colorTwo.red);
}

/**
 *   Scan Line realization of floodFill algo
 *   It consumes less memory and more effective
 */

void scanLineFloodFill(const std::string file, BITMAPFILEHEADER bh, BITMAPINFO bi, RGB **rgbGrid, size_t x, size_t y,
                       RGB newColor) {

    if (y >= bi.biWidth || x >= bi.biHeight) {
        std::cerr << "wrong coordinates : index out of bounds " << std::endl;
        return;
    }

    RGB oldColor = rgbGrid[x][y];

    if (RGB_equals(oldColor, newColor)) {
        std::cerr << " can't floodFill this area, because of color similarity " << std::endl;
        return;
    }

    PointXY currentPoint{x, y};
    std::vector<PointXY> q;
    size_t currentX;
    size_t currentY;

    do {

        if (!q.empty()) {
            q.pop_back();
        }

        currentX = currentPoint.x;
        currentY = currentPoint.y;

        while (currentY > 0 && RGB_equals(rgbGrid[currentX][currentY - 1], oldColor)) {
            currentY--;
        }

        bool spanUp = false;
        bool spanDown = false;

        while (currentY < bi.biWidth && RGB_equals(rgbGrid[currentX][currentY], oldColor)) {

            rgbGrid[currentX][currentY] = newColor;

            if (!spanUp && currentX > 0 && RGB_equals(rgbGrid[currentX - 1][currentY], oldColor)) {
                PointXY p{currentX - 1, currentY};
                q.push_back(p);
                spanUp = true;

            } else if (spanUp && currentX > 0 && !RGB_equals(rgbGrid[currentX - 1][currentY], oldColor)) {
                spanUp = false;
            }

            if (!spanDown && currentX < (bi.biHeight - 1) && RGB_equals(rgbGrid[currentX + 1][currentY], oldColor)) {
                PointXY p{currentX + 1, currentY};
                q.push_back(p);
                spanDown = true;

            } else if (spanDown && currentX < (bi.biHeight - 1) &&
                       !RGB_equals(rgbGrid[currentX + 1][currentY], oldColor)) {
                spanDown = false;
            }

            currentY++;
        }

        if (!q.empty()) {
            currentPoint = q.back();
        }

    } while (!q.empty());

    writeBMP(file, bh, bi, rgbGrid);
}


int main(int argc, char *argv[]) {

    if (argc < 4) {
        std::cerr << "there must be 3 file paths" << std::endl;
        return -1;
    }

    const std::string file1 = argv[1];
    const std::string file2 = argv[2];
    const std::string file3 = argv[3];


    BITMAPFILEHEADER bh;
    BITMAPINFO bi;

    RGB **rgb_arr = loadBMP(file1, bh, bi);

    //xor
    xorBMP(file1,file2,file3);


    //rgb
    int r;
    int g;
    int b;

    //coordinates
    size_t x;
    size_t y;


    std::cout << "Enter rgb colors : " << std::endl;
    std::cin >> r;
    std::cin >> g;
    std::cin >> b;

    std::cout << "Enter coordinates :" << std::endl;
    std::cin >> x;
    std::cin >> y;

    RGB newColor{(uint8_t) b, (uint8_t) g, (uint8_t) r};

    scanLineFloodFill(file1, bh, bi, rgb_arr, y, x, newColor);

    clear(rgb_arr, bi.biHeight);

    return 0;
}


